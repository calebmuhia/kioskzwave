#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

This file is part of **python-openzwave** project http://code.google.com/p/python-openzwave.
    :platform: Unix, Windows, MacOS X
    :sinopsis: openzwave wrapper

.. moduleauthor:: bibi21000 aka Sébastien GALLET <bibi21000@gmail.com>

License : GPL(v3)

**python-openzwave** is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

**python-openzwave** is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with python-openzwave. If not, see http://www.gnu.org/licenses.

"""

import logging
import sys, os
from subprocess import call

from db import get_cameras
pathname = os.path.dirname(sys.argv[0]) 

dir_path = os.path.abspath(pathname)

#logging.getLogger('openzwave').addHandler(logging.NullHandler())
logging.basicConfig(level=logging.DEBUG, filename=os.path.join(dir_path,'kioskzwave.log'), format='%(levelname)s:%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
#logging.basicConfig(level=logging.INFO)

logger = logging.getLogger('zwave')

logger.info('testing')

logger.info('current dir %s' % dir_path)




try :
    import openzwave
    from openzwave.node import ZWaveNode
    from openzwave.value import ZWaveValue
    from openzwave.scene import ZWaveScene
    from openzwave.controller import ZWaveController
    from openzwave.network import ZWaveNetwork
    from openzwave.option import ZWaveOption
    print("Openzwave is installed.")
except :
    print("Openzwave is not installed. Get it from tmp directory.")
    sys.path.insert(0, os.path.abspath('../build/tmp/usr/local/lib/python2.6/dist-packages'))
    sys.path.insert(0, os.path.abspath('../build/tmp/usr/local/lib/python2.7/dist-packages'))
    sys.path.insert(0, os.path.abspath('build/tmp/usr/local/lib/python2.6/dist-packages'))
    sys.path.insert(0, os.path.abspath('build/tmp/usr/local/lib/python2.7/dist-packages'))
    import openzwave
    from openzwave.node import ZWaveNode
    from openzwave.value import ZWaveValue
    from openzwave.scene import ZWaveScene
    from openzwave.controller import ZWaveController
    from openzwave.network import ZWaveNetwork
    from openzwave.option import ZWaveOption
import time
from louie import dispatcher, All

devices=['/dev/ttyUSB1','/dev/ttyUSB2']
log='None'
sniff=60.0

for arg in sys.argv:
    if arg.startswith("--device"):
        temp,device = arg.split("=")
    elif arg.startswith("--log"):
        temp,log = arg.split("=")
    elif arg.startswith("--sniff"):
        temp,sniff = arg.split("=")
        sniff = float(sniff)
    elif arg.startswith("--help"):
        print("help : ")
        print("  --device=/dev/yourdevice ")
        print("  --log=Info|Debug")

#Define some manager options
found = False
print len(devices)
for i in range(len(devices)):
    try:
        options = ZWaveOption(devices[i], \
          config_path="/home/test/code/python-openzwave/openzwave/config", \
          user_path=".", cmd_line="")
        found = True
    except Exception, e:
        print e
        pass 

if not found:
    print "device not found"
    print found
    sys.exit()           


options.set_log_file("OZW_Log.log")
options.set_append_log_file(True)
options.set_console_output(True)
options.set_save_log_level("Debug")
#options.set_save_log_level('Info')
options.set_logging(True)
options.lock()

def triggerzm(value):

    try:

        cameras = get_cameras()

        data = value.split('data: ')[1]
        logger.info('update value = %s' % data)

        if 'True' in data:
            logger.info('sending trigger')
            for camera in cameras:
                cause = 'door-trigger'   
                path = os.path.join(dir_path, "triggers.sh")
                call("bash %s %s %s" % (path, camera, cause), shell=True)
    except Exception,e:
        logger.error(e)     

def louie_network_started(network):
    logger.info('//////////// ZWave network is started ////////////')
    logger.info('Louie signal : OpenZWave network is started : homeid %0.8x - %d nodes were found.' % \
        (network.home_id, network.nodes_count))


def louie_network_resetted(network):
    logger.info('Louie signal : OpenZWave network is resetted.')

def louie_network_ready(network):
    logger.info('//////////// ZWave network is ready ////////////')
    logger.info('Louie signal : ZWave network is ready : %d nodes were found.' % network.nodes_count)
    logger.info('Louie signal : Controller : %s' % network.controller)
    dispatcher.connect(louie_node_update, ZWaveNetwork.SIGNAL_NODE)
    dispatcher.connect(louie_value_update, ZWaveNetwork.SIGNAL_VALUE)
    dispatcher.connect(louie_ctrl_message, ZWaveController.SIGNAL_CONTROLLER)

def louie_node_update(network, node):
    logger.debug('Louie signal : Node update : %s.' % node)
    # triggerzm(node=node)

def louie_value_update(network, node, value):
    logger.debug('Louie signal : Value update : %s.' % value)
    triggerzm(str(value))

def louie_ctrl_message(state, message, network, controller):
    logger.debug('Louie signal : Controller message : %s.' % message)


   

#Create a network object
network = ZWaveNetwork(options, log=None)

dispatcher.connect(louie_network_started, ZWaveNetwork.SIGNAL_NETWORK_STARTED)
dispatcher.connect(louie_network_resetted, ZWaveNetwork.SIGNAL_NETWORK_RESETTED)
dispatcher.connect(louie_network_ready, ZWaveNetwork.SIGNAL_NETWORK_READY)

dispatcher.connect(louie_node_update, ZWaveNetwork.SIGNAL_NODE)
dispatcher.connect(louie_value_update, ZWaveNetwork.SIGNAL_VALUE)

logger.info("------------------------------------------------------------")
logger.info("Waiting for driver : ")
logger.info("------------------------------------------------------------")
for i in range(0,300):
    if network.state>=network.STATE_STARTED:
        logger.info(" done")
        break
    else:
        # sys.stdout.write(".")
        # sys.stdout.flush()
        time.sleep(1.0)
if network.state<network.STATE_STARTED:
    print "."
    print "Can't initialise driver! Look at the logs in OZW_Log.log"
    quit(1)
logger.info("------------------------------------------------------------")
logger.info("Use openzwave library : %s" % network.controller.ozw_library_version)
logger.info("Use python library : %s" % network.controller.python_library_version)
logger.info("Use ZWave library : %s" % network.controller.library_description)
logger.info("Network home id : %s" % network.home_id_str)
logger.info("Controller node id : %s" % network.controller.node.node_id)
logger.info("Controller node version : %s" % (network.controller.node.version))
logger.info("Nodes in network : %s" % network.nodes_count)
logger.info("------------------------------------------------------------")
logger.info("Waiting for network to become ready : ")
logger.info("------------------------------------------------------------")

while True:
    time.sleep(1)

# for i in range(0,300):
#     if network.state>=network.STATE_READY:
#         print " done"
#         break
#     else:
#         sys.stdout.write(".")
#         sys.stdout.flush()
#         time.sleep(1.0)
# if not network.is_ready:
#     print "."
#     print "Can't start network! Look at the logs in OZW_Log.log"

#     quit(2)

# print "------------------------------------------------------------"
# print "Controller capabilities : %s" % network.controller.capabilities
# print "Controller node capabilities : %s" % network.controller.node.capabilities
# print "Nodes in network : %s" % network.nodes_count
# print "Driver statistics : %s" % network.controller.stats
# print "------------------------------------------------------------"

# time.sleep(sniff)

# print
# print "------------------------------------------------------------"
# print "Driver statistics : %s" % network.controller.stats
# print "------------------------------------------------------------"

# print
# print "------------------------------------------------------------"
# print "Stop network"
# print "------------------------------------------------------------"
# network.stop()
